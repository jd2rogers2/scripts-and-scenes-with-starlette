from httpx import AsyncClient
import re
import pytest

from app import app


mock_story = {
    "title": "story show test",
    "creator": "james",
    "year_released": 1990,
    "medium": "book",
}

@pytest.mark.anyio
async def test_story_show():
    async with AsyncClient(app=app, base_url="http://localhost:8000") as client:
        r1 = await client.post("/stories/", data=mock_story)
        match = re.search(r'/stories/(\d|[a-zA-Z]){24}', str(r1.next_request.url))
        id_bounds = match.span()
        created_id = str(r1.next_request.url)[id_bounds[0]:id_bounds[1]].replace('/stories/', '')
        r2 = await client.get(f"/stories/{created_id}")
        assert r2.status_code == 200
        assert f'title: {mock_story["title"]}' in str(r2.content), "displays the story's title"
        assert f'creator: {mock_story["creator"]}' in str(r2.content), "displays the story's creator"
        assert f'year released: {mock_story["year_released"]}' in str(r2.content), "displays the story's year released"
        assert f'medium: {mock_story["medium"]}' in str(r2.content), "displays the story's medium"
