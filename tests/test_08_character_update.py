from httpx import AsyncClient
import pytest
import re

from app import app


mock_story = {
    "title": "startup",
    "creator": "	Park Hye-ryun",
    "year_released": 2020,
    "medium": "tv series",
}

mock_character_init = {
    'name': 'Mr. Han',
    'age': 31,
    'description': "Good boy and VC with perfect record. Not interested in Dal-mi",
}

mock_character_updated = {
    'name': 'Mr. Han',
    'age': 31,
    'description': "Good boy and VC with perfect record. Definitely has a crush Dal-mi",
}

@pytest.fixture
async def story_id():
    async with AsyncClient(app=app, base_url="http://localhost:8000") as client:
        r1 = await client.post("/stories/", data=mock_story)
        match = re.search(r'/stories/(\d|[a-zA-Z]){24}', str(r1.next_request.url))
        id_bounds = match.span()
        id = str(r1.next_request.url)[id_bounds[0]:id_bounds[1]].replace('/stories/', '')
        return id

@pytest.mark.anyio
async def test_character_update(story_id):
    async with AsyncClient(app=app, base_url="http://localhost:8000") as client:
        r1 = await client.post(f"/stories/{story_id}/characters/", data=mock_character_init)
        match = re.search(r'/stories/(\d|[a-zA-Z]){24}/characters/(\d|[a-zA-Z]){24}', str(r1.next_request.url))
        id_bounds = match.span()
        created_id = str(r1.next_request.url)[id_bounds[0]:id_bounds[1]].replace(f'/stories/{story_id}/characters/', '')

        r2 = await client.post(f"/stories/{story_id}/characters/{created_id}", data=mock_character_updated)
        assert r2.status_code == 302
        assert f"/stories/{story_id}/characters/{created_id}" in str(r2.next_request.url), "update endpoint redirects to character show page"

        r3 = await client.get(f"/stories/{story_id}/characters/{created_id}")
        assert r3.status_code == 200
        html = str(r3.content)
        assert f'description: {mock_character_updated["description"]}' in html, "displays the character's updated description"
