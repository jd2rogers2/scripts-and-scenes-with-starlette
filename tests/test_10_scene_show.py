from httpx import AsyncClient
import pytest
import re

from app import app


mock_story = {
    "title": "startup",
    "creator": "	Park Hye-ryun",
    "year_released": 2020,
    "medium": "tv series",
}

mock_character = {
    'name': 'Mr. Han',
    'age': 16,
    'description': "Boy genius with a big heart but also looks out for number 1",
}

@pytest.fixture
async def story_id():
    async with AsyncClient(app=app, base_url="http://localhost:8000") as client:
        r1 = await client.post("/stories/", data=mock_story)
        match = re.search(r'/stories/(\d|[a-zA-Z]){24}', str(r1.next_request.url))
        id_bounds = match.span()
        id = str(r1.next_request.url)[id_bounds[0]:id_bounds[1]].replace('/stories/', '')
        return id

@pytest.fixture
async def character_id(story_id):
    async with AsyncClient(app=app, base_url="http://localhost:8000") as client:
        r1 = await client.post(f"/stories/{story_id}/characters/", data=mock_character)
        match = re.search(r'/stories/(\d|[a-zA-Z]){24}/characters/(\d|[a-zA-Z]){24}', str(r1.next_request.url))
        id_bounds = match.span()
        id = str(r1.next_request.url)[id_bounds[0]:id_bounds[1]].replace(f'/stories/{story_id}/characters/', '')
        return id

@pytest.mark.anyio
async def test_scene_show(story_id, character_id):
    async with AsyncClient(app=app, base_url="http://localhost:8000") as client:
        mock_scene = {
            'rank': 2,
            'plot': "Mr. Han won an investing competition but has nowhere to live so halmoni takes him in",
            'setting': 'seoul streets outside the corn dog shop',
            'character_ids[]': character_id,
        }
        r1 = await client.post(f"/stories/{story_id}/scenes/", data=mock_scene)
        match = re.search(r'/stories/(\d|[a-zA-Z]){24}/scenes/(\d|[a-zA-Z]){24}', str(r1.next_request.url))
        id_bounds = match.span()
        scene_id = str(r1.next_request.url)[id_bounds[0]:id_bounds[1]].replace(f'/stories/{story_id}/scenes/', '')

        r2 = await client.get(f"/stories/{story_id}/scenes/{scene_id}")
        html = str(r2.content)
        assert r2.status_code == 200
        assert f'scene: {mock_scene["rank"]}' in html, "displays the scene's rank"
        assert f'setting: {mock_scene["setting"]}' in html, "displays the scene's setting"
        assert f'plot: {mock_scene["plot"]}' in html, "displays the scene's plot"
        assert f'<li>{mock_character["name"]}</li>' in html, "displays the names of the scene's characters"
        assert f'<a href="/stories/{story_id}/scenes/{scene_id}/edit"' in html, "has a link to the edit page"
        assert f'<form method="POST" action="/stories/{story_id}/scenes/{scene_id}/delete"' in html, "has a delete form"
        assert f'<input type="submit" value="Delete' in html, "and a delete button"
