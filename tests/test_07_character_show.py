from httpx import AsyncClient
import pytest
import re

from app import app


mock_story = {
    "title": "startup",
    "creator": "	Park Hye-ryun",
    "year_released": 2020,
    "medium": "tv series",
}

mock_character = {
    'name': 'Won In Jae',
    'age': 28,
    'description': "the sister who chose mom and the rich step dad",
}

@pytest.fixture
async def story_id():
    async with AsyncClient(app=app, base_url="http://localhost:8000") as client:
        r1 = await client.post("/stories/", data=mock_story)
        match = re.search(r'/stories/(\d|[a-zA-Z]){24}', str(r1.next_request.url))
        id_bounds = match.span()
        id = str(r1.next_request.url)[id_bounds[0]:id_bounds[1]].replace('/stories/', '')
        return id

@pytest.mark.anyio
async def test_character_show(story_id):
    async with AsyncClient(app=app, base_url="http://localhost:8000") as client:
        r1 = await client.post(f"/stories/{story_id}/characters/", data=mock_character)
        match = re.search(r'/stories/(\d|[a-zA-Z]){24}/characters/(\d|[a-zA-Z]){24}', str(r1.next_request.url))
        id_bounds = match.span()
        character_id = str(r1.next_request.url)[id_bounds[0]:id_bounds[1]].replace(f'/stories/{story_id}/characters/', '')

        r2 = await client.get(f"/stories/{story_id}/characters/{character_id}")
        assert r2.status_code == 200
        assert f'name: {mock_character["name"]}' in str(r2.content), "displays the character's name"
        assert f'age: {mock_character["age"]}' in str(r2.content), "displays the character's age"
        assert f'description: {mock_character["description"]}' in str(r2.content), "displays the character's description"
        assert f'<a href="/stories/{story_id}/characters/{character_id}/edit"' in str(r2.content), "has a link to the edit page"
        assert f'<form method="POST" action="/stories/{story_id}/characters/{character_id}/delete"' in str(r2.content), "has a delete form"
        assert f'<input type="submit" value="Delete"' in str(r2.content), "and a delete button"
