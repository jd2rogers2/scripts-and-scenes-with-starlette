from httpx import AsyncClient
import pytest
import re

from app import app


mock_story = {
    "title": "startup",
    "creator": "	Park Hye-ryun",
    "year_released": 2020,
    "medium": "tv series",
}

mock_character = {
    'name': 'Mr. Han',
    'age': 16,
    'description': "Boy genius with a big heart but also looks out for number 1",
}

@pytest.fixture
async def story_id():
    async with AsyncClient(app=app, base_url="http://localhost:8000") as client:
        r1 = await client.post("/stories/", data=mock_story)
        match = re.search(r'/stories/(\d|[a-zA-Z]){24}', str(r1.next_request.url))
        id_bounds = match.span()
        id = str(r1.next_request.url)[id_bounds[0]:id_bounds[1]].replace('/stories/', '')
        return id

@pytest.fixture
async def character_id(story_id):
    async with AsyncClient(app=app, base_url="http://localhost:8000") as client:
        r1 = await client.post(f"/stories/{story_id}/characters/", data=mock_character)
        match = re.search(r'/stories/(\d|[a-zA-Z]){24}/characters/(\d|[a-zA-Z]){24}', str(r1.next_request.url))
        id_bounds = match.span()
        id = str(r1.next_request.url)[id_bounds[0]:id_bounds[1]].replace(f'/stories/{story_id}/characters/', '')
        return id

@pytest.mark.anyio
async def test_scene_create_form(story_id, character_id):
    async with AsyncClient(app=app, base_url="http://localhost:8000") as client:
        r = await client.get(f"/stories/{story_id}/scenes/create")
        assert r.status_code == 200
        assert '<form' in str(r.content), "there is a form in the template"
        assert 'method="POST"' in str(r.content), "the form's method is POST"
        assert f'action="/stories/{story_id}/scenes' in str(r.content), "the form hits the /stories/:story_id/scenes endpoint"
        assert '<input name="rank" type="number"' in str(r.content), "has a rank input"
        assert '<input name="setting" type="text"' in str(r.content), "has an setting input"
        assert '<textarea name="plot' in str(r.content), "has a plot textarea"
        assert f'<input type="checkbox" id="{character_id}" name="character_ids[]" value="{character_id}" />' in str(r.content), "has checkboxes for all of this stories characters"
        assert '<input type="submit"' in str(r.content), "the form has a submit input"

@pytest.mark.anyio
async def test_scene_create_endpoint(story_id, character_id):
    async with AsyncClient(app=app, base_url="http://localhost:8000") as client:
        mock_scene = {
            'rank': 2,
            'plot': "Mr. Han won an investing competition but has nowhere to live so halmoni takes him in <3",
            'setting': 'seoul streets outside the corn dog shop',
            'character_ids[]': character_id,
        }
        r = await client.post(f"/stories/{story_id}/scenes/", data=mock_scene)
        assert r.status_code == 302
        parsed_url = re.search(r'/stories/(\d|[a-zA-Z]){24}/scenes/(\d|[a-zA-Z]){24}', str(r.next_request.url))
        assert parsed_url is not None, "post endpoint redirects to url like /stories/:story_id/scenes/:scene_id"
