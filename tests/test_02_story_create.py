from httpx import AsyncClient
from starlette.testclient import TestClient
import pytest
import re

from app import app


def test_story_create_form():
    client = TestClient(app)
    response = client.get("/stories/create")
    assert response.status_code == 200
    assert response.template.name == 'stories/templates/form.html'
    assert "request" in response.context
    assert '<form' in str(response.content), "there is a form in the template"
    assert 'method="POST"' in str(response.content), "the form's method is POST"
    assert 'action="/stories/"' in str(response.content), "the form hits the /stories/ endpoint"
    assert '<input type="submit"' in str(response.content), "the form has a submit input"

@pytest.mark.anyio
async def test_story_create_endpoint():
    async with AsyncClient(app=app, base_url="http://localhost:8000") as client:
        mock_story = {
            "title": "story create test",
            "creator": "james",
            "year_released": 1990,
            "medium": "book",
        }
        r = await client.post("/stories/", data=mock_story)
        assert r.status_code == 302
        parsed_url = re.search(r'/stories/(\d|[a-zA-Z]){24}', str(r.next_request.url))
        assert parsed_url is not None, "post endpoint redirects to url like /stories/:story_id"
