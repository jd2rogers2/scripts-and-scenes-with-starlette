from httpx import AsyncClient
import re
import pytest

from app import app


mock_stories = [
    {
        "title": "story 1",
        "creator": "james",
        "year_released": 1990,
        "medium": "book",
    },
    {
        "title": "story 2",
        "creator": "james",
        "year_released": 2023,
        "medium": "book",
    },
]

@pytest.mark.anyio
async def test_story_list_view():
    async with AsyncClient(app=app, base_url="http://localhost:8000") as client:
        await client.post("/stories/", data=mock_stories[0])
        await client.post("/stories/", data=mock_stories[1])
        req = await client.get(f"/stories/")
        assert req.status_code == 200
        for story in mock_stories:
            assert f'{story["title"]}' in str(req.content), "displays the stories' titles"

        matches_count = len(re.findall('<a href="/stories/(\d|[a-zA-Z]){24}', str(req.content)))
        assert matches_count >= 2, "displays the links to the story show pages"
