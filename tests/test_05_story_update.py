from httpx import AsyncClient
import re
import pytest

from app import app


mock_story_init = {
    "title": "story update test init",
    "creator": "james",
    "year_released": 1990,
    "medium": "book",
}

mock_story_updated = {
    "title": "story update test updated",
    "creator": "james",
    "year_released": 2023,
    "medium": "book",
}

@pytest.mark.anyio
async def test_story_update():
    async with AsyncClient(app=app, base_url="http://localhost:8000") as client:
        r1 = await client.post("/stories/", data=mock_story_init)
        match = re.search(r'/stories/(\d|[a-zA-Z]){24}', str(r1.next_request.url))
        id_bounds = match.span()
        created_id = str(r1.next_request.url)[id_bounds[0]:id_bounds[1]].replace('/stories/', '')

        r2 = await client.get(f"/stories/{created_id}/edit")
        html = str(r2.content)
        assert f'<form method="POST" action="/stories/{created_id}"' in html, "displays a form with method POST and action to /story/:story_id"
        assert f'<input name="title" type="text" value="{mock_story_init["title"]}"' in html, "displays a title input, with the default value of the story's title"
        assert f'<input name="creator" type="text" value="{mock_story_init["creator"]}"' in html, "displays an creator input, with the default value of the story's creator"
        assert f'<input name="year_released" type="number" value="{mock_story_init["year_released"]}"' in html, "displays the year released input, with the default value of the story's release year"
        assert '<input type="submit"' in html, "the form has a submit input"

        r3 = await client.post(f"/stories/{created_id}", data=mock_story_updated)
        assert r3.status_code == 302
        assert f"/stories/{created_id}" in str(r3.next_request.url), "update endpoint redirects to story show page"

        r4 = await client.get(f"/stories/{created_id}")
        assert r4.status_code == 200
        html = str(r4.content)
        assert f'title: {mock_story_updated["title"]}' in html, "displays the updated story's title"
        assert f'year released: {mock_story_updated["year_released"]}' in html, "displays the story's year released"
