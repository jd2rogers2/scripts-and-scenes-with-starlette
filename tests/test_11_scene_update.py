from httpx import AsyncClient
from bson import ObjectId
import pytest
import re

from app import app


mock_story = {
    "title": "startup",
    "creator": "	Park Hye-ryun",
    "year_released": 2020,
    "medium": "tv series",
}

mock_character = {
    'name': 'Lee Chul-san',
    'age': 25,
    'description': "Dosan's friend from school, loud, emotional",
}

@pytest.fixture
async def story_id():
    async with AsyncClient(app=app, base_url="http://localhost:8000") as client:
        r1 = await client.post("/stories/", data=mock_story)
        match = re.search(r'/stories/(\d|[a-zA-Z]){24}', str(r1.next_request.url))
        id_bounds = match.span()
        id = str(r1.next_request.url)[id_bounds[0]:id_bounds[1]].replace('/stories/', '')
        return id

@pytest.fixture
async def character_id(story_id):
    async with AsyncClient(app=app, base_url="http://localhost:8000") as client:
        r1 = await client.post(f"/stories/{story_id}/characters/", data=mock_character)
        match = re.search(r'/stories/(\d|[a-zA-Z]){24}/characters/(\d|[a-zA-Z]){24}', str(r1.next_request.url))
        id_bounds = match.span()
        id = str(r1.next_request.url)[id_bounds[0]:id_bounds[1]].replace(f'/stories/{story_id}/characters/', '')
        return id

@pytest.mark.anyio
async def test_scene_update(story_id, character_id):
    async with AsyncClient(app=app, base_url="http://localhost:8000") as client:
        mock_scene_init = {
            'rank': 42,
            'plot': "Samsan tech impresses with their hackathon presentation",
            'setting': 'sandbox campus',
            'character_ids[]': character_id,
        }

        r1 = await client.post(f"/stories/{story_id}/scenes/", data=mock_scene_init)
        match = re.search(r'/stories/(\d|[a-zA-Z]){24}/scenes/(\d|[a-zA-Z]){24}', str(r1.next_request.url))
        id_bounds = match.span()
        created_id = str(r1.next_request.url)[id_bounds[0]:id_bounds[1]].replace(f'/stories/{story_id}/scenes/', '')

        mock_scene_updated = {
            'rank': 42,
            'plot': "Samsan tech impresses with their hackathon presentation, but then ai fonts from Injae company out do their algo",
            'setting': 'sandbox campus',
            'character_ids[]': character_id,
        }
        r2 = await client.post(f"/stories/{story_id}/scenes/{created_id}", data=mock_scene_updated)
        assert r2.status_code == 302
        assert f"/stories/{story_id}/scenes/{created_id}" in str(r2.next_request.url), "update endpoint redirects to scene show page"

        r3 = await client.get(f"/stories/{story_id}/scenes/{created_id}")
        assert r3.status_code == 200
        html = str(r3.content)
        assert f'plot: {mock_scene_updated["plot"]}' in html, "displays the scene's updated plot"
