from httpx import AsyncClient
from starlette.testclient import TestClient
import pytest

from app import app


def test_home_route_exists():
    client = TestClient(app)
    response = client.get("/")
    assert response.status_code == 200

def test_home_route_template():
    client = TestClient(app)
    response = client.get("/")
    assert response.template.name == 'base.html'
    assert "request" in response.context

@pytest.mark.anyio
async def test_home_route_template_contents() -> None:
    async with AsyncClient(app=app, base_url="http://localhost:8000") as client:
        r = await client.get("/")
        assert r.status_code == 200
        assert '<img class="logo"' in str(r.content)
        assert '<h1 class="app_name"' in str(r.content)
        assert '<div class="nav"' in str(r.content)

