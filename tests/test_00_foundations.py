from bson import ObjectId
import os


def test_file_structure():
    assert os.path.isfile('./db_connection.py'), "a db_connection.py file exists in the root"
    assert os.path.isfile('./base.html'), "a base.html template file exists in the root"
    assert os.path.isfile('./app.py'), "an app.py file exists in the root"

    # stories
    assert os.path.isfile('./stories/controller.py'), "a controller.py file exists in the stories directory"
    assert os.path.isfile('./stories/models.py'), "a models.py file exists in the stories directory"
    assert os.path.isfile('./stories/routes.py'), "a routes.py file exists in the stories directory"

    assert os.path.isfile('./stories/templates/form.html'), "a form template file exists in the stories/templates directory"
    assert os.path.isfile('./stories/templates/list.html'), "a list template file exists in the stories/templates directory"
    assert os.path.isfile('./stories/templates/show.html'), "a show template file exists in the stories/templates directory"
    assert os.path.isfile('./stories/templates/update.html'), "an update template file exists in the stories/templates directory"

    # characters
    assert os.path.isfile('./characters/controller.py'), "a controller.py file exists in the characters directory"
    assert os.path.isfile('./characters/models.py'), "a models.py file exists in the characters directory"
    assert os.path.isfile('./characters/routes.py'), "a routes.py file exists in the characters directory"

    assert os.path.isfile('./characters/templates/form.html'), "a form template file exists in the characters/templates directory"
    assert os.path.isfile('./characters/templates/show.html'), "a show template file exists in the characters/templates directory"
    assert os.path.isfile('./characters/templates/update.html'), "an update template file exists in the characters/templates directory"

    # scenes
    assert os.path.isfile('./scenes/controller.py'), "a controller.py file exists in the scenes directory"
    assert os.path.isfile('./scenes/models.py'), "a models.py file exists in the scenes directory"
    assert os.path.isfile('./scenes/routes.py'), "a routes.py file exists in the scenes directory"

    assert os.path.isfile('./scenes/templates/form.html'), "a form template file exists in the scenes/templates directory"
    assert os.path.isfile('./scenes/templates/show.html'), "a show template file exists in the scenes/templates directory"
    assert os.path.isfile('./scenes/templates/update.html'), "an update template file exists in the scenes/templates directory"

def test_root_imports():
    try:
        from db_connection import collection
        assert type(collection).__name__ == 'Collection', "the collection variable is a result of getting a MongoClient's collection"
    except ImportError:
        assert False, "a collection variable must exist in db_connection.py"

    try:
        from app import app
        assert type(app).__name__ == 'Starlette', "the app variable is an instance of a Starlette server"
    except ImportError:
        assert False, "an app variable must exist in app.py"

def test_story_imports():
    try:
        from stories.routes import routes
    except ImportError:
        assert False, "a routes variable must exist in stories/routes.py"

    try:
        from stories.models import Story
    except ImportError:
        assert False, "an Story class must exist in stories/models.py"

def test_story_model():
    from stories.models import Story
    story_id = ObjectId()
    story_data = { "_id": story_id, "title": "test", "year_released": 2023, "creator": "james", "medium": 'book', "extra": 1 }
    story = Story(**story_data)
    assert story.title == story_data["title"]
    assert story.year_released == story_data["year_released"]
    assert story.creator == story_data["creator"]
    assert story.medium == story_data["medium"]
    assert story._id == story_id
    try:
        assert story.extra == None
    except AttributeError as ae:
        assert str(ae) == "'Story' object has no attribute 'extra'", "the Story class must not assign any other properties than title, year_released, creator, medium, and optionally _id"

    del story_data["_id"]
    story = Story(**story_data)
    assert story._id is not None, "by default creates an _id"

def test_character_imports():
    try:
        from characters.routes import routes
    except ImportError:
        assert False, "a routes variable must exist in characters/routes.py"

    try:
        from characters.models import Character
    except ImportError:
        assert False, "a Character class must exist in characters/models.py"

def test_character_model():
    from characters.models import Character
    char_id = ObjectId()
    character_data = { "_id": char_id, "name": "james", "age": 33, "description": "instructor", "extra": 1 }
    character = Character(**character_data)
    assert character.name == character_data["name"]
    assert character.age == character_data["age"]
    assert character.description == character_data["description"]
    assert character._id == char_id
    try:
        assert character.extra == None
    except AttributeError as ae:
        assert str(ae) == "'Character' object has no attribute 'extra'", "the Character class must not assign any other properties than title, year_released, creator, medium, and optionally _id"

    del character_data["_id"]
    character = Character(**character_data)
    assert character._id is not None, "by default creates an _id"

def test_scene_imports():
    try:
        from scenes.routes import routes
    except ImportError:
        assert False, "a routes variable must exist in scenes/routes.py"

    try:
        from scenes.models import Scene
    except ImportError:
        assert False, "a Scene class must exist in scenes/models.py"

def test_scene_model():
    from scenes.models import Scene
    scene_id = ObjectId()
    scene_data = { "_id": scene_id, "rank": 1, "plot": "some stuff happened", "setting": "that place", "character_ids": [ObjectId()], "extra": 1 }
    scene = Scene(**scene_data)
    assert scene.rank == scene_data["rank"]
    assert scene.plot == scene_data["plot"]
    assert scene.setting == scene_data["setting"]
    assert scene.character_ids == scene_data["character_ids"]
    assert scene._id == scene_id
    try:
        assert scene.extra == None
    except AttributeError as ae:
        assert str(ae) == "'Scene' object has no attribute 'extra'", "the scene class must not assign any other properties than title, year_released, creator, medium, and optionally _id"

    del scene_data["_id"]
    scene = Scene(**scene_data)
    assert scene._id is not None, "by default creates an _id"