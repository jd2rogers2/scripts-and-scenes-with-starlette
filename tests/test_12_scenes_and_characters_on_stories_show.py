from httpx import AsyncClient
from starlette.testclient import TestClient
import pytest
import re

from app import app


mock_story = {
    "title": "startup",
    "creator": "	Park Hye-ryun",
    "year_released": 2020,
    "medium": "tv series",
}

mock_character = {
    "name": "Seo Dal-mi",
    "age": 10,
    "description": "younger sister, scrappy, hard worker",
}

@pytest.mark.anyio
async def test_scenes_and_characters_on_stories_show():
    async with AsyncClient(app=app, base_url="http://localhost:8000") as client:
        r1 = await client.post("/stories/", data=mock_story)
        match = re.search(r'/stories/(\d|[a-zA-Z]){24}', str(r1.next_request.url))
        id_bounds = match.span()
        story_id = str(r1.next_request.url)[id_bounds[0]:id_bounds[1]].replace('/stories/', '')

        r2 = await client.post(f"/stories/{story_id}/characters/", data=mock_character)
        match = re.search(r'/characters/(\d|[a-zA-Z]){24}', str(r2.next_request.url))
        id_bounds = match.span()
        character_id = str(r2.next_request.url)[id_bounds[0]:id_bounds[1]].replace('/characters/', '')

        mock_scene = {
            "rank": 1,
            "plot": "the sisters parents are breaking up and they have to choose a side",
            "setting": "seoul",
            "character_ids[]": character_id
        }
        r3 = await client.post(f"/stories/{story_id}/scenes/", data=mock_scene)
        match = re.search(r'/scenes/(\d|[a-zA-Z]){24}', str(r3.next_request.url))
        id_bounds = match.span()
        scene_id = str(r3.next_request.url)[id_bounds[0]:id_bounds[1]].replace(f'/scenes/', '')

        r4 = await client.get(f"/stories/{story_id}")
        assert r4.status_code == 200
        assert f'<a href="/stories/{story_id}/scenes/{scene_id}/' in str(r4.content), "displays a list of links for the story's scenes on the show page"
        assert f'<a href="/stories/{story_id}/scenes/create"' in str(r4.content), "displays a link to add new scenes page"
        assert f'<a href="/stories/{story_id}/characters/{character_id}/">' in str(r4.content), "displays a list of links for the story's scenes on the show page"
        assert f'<a href="/stories/{story_id}/characters/create"' in str(r4.content), "displays a list of links for the story's characters on the show page"
