from httpx import AsyncClient
import pytest
import re

from app import app


mock_story = {
    "title": "startup",
    "creator": "	Park Hye-ryun",
    "year_released": 2020,
    "medium": "tv series",
}

mock_character = {
    'name': 'Won In-jae',
    'age': 28,
    'description': "the sister who chose mom and the rich step dad",
}

@pytest.fixture
async def story_id():
    async with AsyncClient(app=app, base_url="http://localhost:8000") as client:
        r1 = await client.post("/stories/", data=mock_story)
        match = re.search(r'/stories/(\d|[a-zA-Z]){24}', str(r1.next_request.url))
        id_bounds = match.span()
        id = str(r1.next_request.url)[id_bounds[0]:id_bounds[1]].replace('/stories/', '')
        return id

@pytest.mark.anyio
async def test_character_create_form(story_id):
    async with AsyncClient(app=app, base_url="http://localhost:8000") as client:
        r = await client.get(f"/stories/{story_id}/characters/create")
        assert r.status_code == 200
        assert '<form' in str(r.content), "there is a form in the template"
        assert 'method="POST"' in str(r.content), "the form's method is POST"
        assert f'action="/stories/{story_id}/characters' in str(r.content), "the form hits the /stories/:story_id/characters endpoint"
        assert '<input name="name" type="text"' in str(r.content), "has a name input"
        assert '<input name="age" type="number"' in str(r.content), "has an age input"
        assert '<textarea name="description' in str(r.content), "has a description textarea"
        assert '<input type="submit"' in str(r.content), "the form has a submit input"

@pytest.mark.anyio
async def test_character_create_endpoint(story_id):
    async with AsyncClient(app=app, base_url="http://localhost:8000") as client:
        r = await client.post(f"/stories/{story_id}/characters/", data=mock_character)
        assert r.status_code == 302
        parsed_url = re.search(r'/stories/(\d|[a-zA-Z]){24}/characters/(\d|[a-zA-Z]){24}', str(r.next_request.url))
        assert parsed_url is not None, "post endpoint redirects to url like /stories/:story_id/characters/:character_id"
