# scripts-and-scenes-with-starlette



## data schema

stories = [
    {
        _id: ObjectId,
        title: str,
        creator: str, # author, director, whatever
        year_released: int,
        medium: str, # book, movie, tv series, manga, etc.
        characters: [
            {
                _id: ObjectId,
                name: str,
                age: int,
                description: str,
            },
            ...
        ],
        scenes: [
            {
                _id: ObjectId,
                rank: int,
                setting: str,
                plot: str, # dialogue, actions, etc.
                character_ids: list[ObjectId],
            },
            ...
        ],
    },
    ...
]




## Getting started

1. create a python virtual environment
2. `pip install -r requirements.txt`
3. `MONGODB_URL='<connection_string>' python -m pytest -s`
4. use Test Driven Development (TDD) to see and fix the errors
5. follow along with the instructions in Learn for context
6. start with a stub by creating a Starlette app in an app.py file in the root of this project
    6.1. follow [starlette.io](https://www.starlette.io/) guide for an example
7. run the site with `python -m uvicorn --reload --env-file .env app:app` to see your existing UI


